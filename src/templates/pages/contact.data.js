const nyOffice = {
  location_name: 'New York City',
  contact_info: {
    general: `+1 212 792 8930 \nhello@red-peak.com`,
    press: `press@red-peak.com`,
    location: `RedPeak\n 625 Broadway, 2nd Floor \nNew York, NY 10012`
  },
  map_embed: `<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3023.6349477314693!2d-73.99872748459465!3d40.7260520793303!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2598ee04db7b3%3A0x617d31632a3dcea3!2sRed+Peak+Branding!5e0!3m2!1sen!2sph!4v1532103731014" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>`,
  image: `https://marvel-live.freetls.fastly.net/canvas/2018/7/9397cb44b35f4072a87aba40c2498183?quality=80&fake=.png`,
};

const taipeiOffice = {
  location_name: 'Taipei',
}


module.exports = {

  title: 'Contact',

  context: {
    title: 'Contact',
    description: 'Contact Page Template',
    hero_text: 'We work hard to understand the people we serve, using strategy and design to create thoughtful experiences for the moments that matter. ',
    locations: [
      nyOffice,
      taipeiOffice,
    ],
    team: [
      {
        name: `Michael Birkin`,
        job_title: `Founder`,
        bio: `Michael Birkin is currently CEO of kyu, the strategic operating unit of Hakuhodo DY Holdings (HDY), and the founder of RedPeak. He began his marketing career as CEO of brand consultancy Interbrand and was a pioneer of brand valuation. After Omnicon Group acquired Interbrand in 1993, Mr. Birkin joined the holding company and quickly rose through the organization, ultimately assuming the roles of President of their largest division, Diversified Agency Services, Chairman/CEO of Omnicom Asia Pacific, and Vice-Chairman of Omnicom Group.`,
        image: `https://marvel-live.freetls.fastly.net/canvas/2018/7/a3308b1e316943ab8b7557d88d186c8c?quality=95&fake=.png`,
      },
      {
        name: `Michael Birkin`,
        job_title: `Founder`,
        bio: `Michael Birkin is currently CEO of kyu, the strategic operating unit of Hakuhodo DY Holdings (HDY), and the founder of RedPeak. He began his marketing career as CEO of brand consultancy Interbrand and was a pioneer of brand valuation. After Omnicon Group acquired Interbrand in 1993, Mr. Birkin joined the holding company and quickly rose through the organization, ultimately assuming the roles of President of their largest division, Diversified Agency Services, Chairman/CEO of Omnicom Asia Pacific, and Vice-Chairman of Omnicom Group.`,
        image: `https://marvel-live.freetls.fastly.net/canvas/2018/7/a3308b1e316943ab8b7557d88d186c8c?quality=95&fake=.png`,
      },
      {
        name: `Michael Birkin`,
        job_title: `Founder`,
        image: `https://marvel-live.freetls.fastly.net/canvas/2018/7/a3308b1e316943ab8b7557d88d186c8c?quality=95&fake=.png`,
      },
      {
        name: `Michael Birkin`,
        job_title: `Founder`,
        image: `https://marvel-live.freetls.fastly.net/canvas/2018/7/a3308b1e316943ab8b7557d88d186c8c?quality=95&fake=.png`,
      },
      {
        name: `Michael Birkin`,
        job_title: `Founder`,
        image: `https://marvel-live.freetls.fastly.net/canvas/2018/7/a3308b1e316943ab8b7557d88d186c8c?quality=95&fake=.png`,
      },
    ],
    job_openings: [
      {
        job_title: `Junior Designer`,
        job_description: `Responsible for generating conceptual and visual design solutions and have an understanding of branding and corporate identity`,
        deadline: `August 1, 2018`,
        linkedin_url: `http://www.google.com`,
      },
      {
        job_title: `Senior Designer`,
        job_description: `Responsible for generating conceptual and visual design solutions and have an understanding of branding and corporate identity`,
        deadline: `August 1, 2018`,
        linkedin_url: `http://www.google.com`,
      },
    ]
  }
};
