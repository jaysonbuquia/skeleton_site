import Collapsible from '../../scripts/modules/Collapsible';

export default {

  instances: [],

  init() {
    const _this = this;

    $('.rp_team-list').each((index, element) => {

      $(element)
      .addClass('rp_accordion')
      .find('.rp_team-list__item--with-bio').each( (index, element) => {
        _this.instances.push(new Collapsible(element, {
          toggle: '.rp_team-list__item__toggle',
          body: '.rp_team-list__item__copy',
        }));
      });

    });
  }

}
