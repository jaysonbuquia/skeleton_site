// Project details modules
const modules = [
  {
    template: `expandable.twig`,
    data: require(`./expandable.data.js`).context || {},
  },
  {
    template: `image-breaker.twig`,
    data: require(`./image-breaker.data.js`).context || {},
  },
  {
    template: `image-1up.twig`,
    data: {
      title: 'Image 1-Up',
      image: 'http://www.placehold.it/1680x1115',
    }
  },
  {
    template: `text-breaker.twig`,
    data: require(`./text-breaker.data.js`).context || {},
  },
  {
    template: `image-2up.twig`,
    data: require(`./image-2up.data.js`).context || {},
  },
  {
    template: `image-1up.twig`,
    data: require(`./image-1up.data.js`).context || {},
  },
  {
    template: `image-1up.twig`,
    data: {
      title: 'Image 1-Up',
      image: 'http://www.placehold.it/1680x1115',
    }
  }
];

module.exports = {

  title: 'Project Detail',

  context: {
    title: 'Project Detail',
    description: 'All Projects Page Template',
    project: {
      title: 'Move On',
      modules: modules,
    }
  }
};
