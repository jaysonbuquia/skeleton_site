module.exports = {

  title: 'Expandable',

  context: {
    title: 'Expandable',
    collapsed_button_text: 'View Details',
    expanded_button_text: 'Hide Details',
    overview: `One sentence overiew of what the project is about. For example we created a new brand to help blah blah blah.`,
    body: `
      <p>Lorem ipsum dolor amet four dollar toast 3 wolf moon waistcoat, pug ennui activated charcoal street art palo santo woke semiotics pour-over meditation cornhole fam butcher. Succulents locavore edison bulb sartorial pour-over tacos kickstarter, fashion axe food truck vinyl meh pop-up thundercats master cleanse post-ironic.</p>
      <p>  Microdosing quinoa poutine keytar fashion axe master cleanse distillery. Hoodie asymmetrical deep v, XOXO iPhone PBR&B salvia normcore disrupt authentic keffiyeh tumeric. Tousled irony hell of asymmetrical. Seitan meh vexillologist typewriter wolf echo park iPhone ramps kale chips. Literally messenger bag authentic chicharrones, kale chips typewriter photo booth flexitarian palo santo pok pok shoreditch offal taiyaki meditation.</p>
      <p>  Hella selvage lomo, crucifix semiotics post-ironic gentrify. Woke listicle glossier, everyday carry normcore cred tumeric organic sartorial keffiyeh farm-to-table forage pork belly next level iPhone. </p>
    `,
    side: `
      <h4>Deliverables</h4>
      <ul>
        <li>Research</li>
        <li>Strategic Positioning </li>
        <li>Brand Narrative </li>
        <li>Core Values </li>
        <li>Personality </li>
        <li>Name & Tagline </li>
        <li>Visual Identity </li>
        <li>Visual System Design </li>
        <li>Animation </li>
        <li>Website Design </li>
        <li>Information Architecture</li>
      </ul>
    `
  }
}
