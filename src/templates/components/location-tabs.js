import Tabs from '../../scripts/modules/Tabs';

export default {

  instances: [],

  init() {

    $('.rp_location-tabs').each((index, element) => {

      let _tabs = new Tabs(element, {
        tab: '.rp_location-tabs__tab',
        pane: '.rp_location-tabs__pane',
      });

      this.instances.push(_tabs);

    });

  }

}
