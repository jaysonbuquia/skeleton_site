module.exports = {

  title: 'Navigation',

  context: {
    items: [
      {
        title: 'All Projects',
        url: '/pages/projects',
      },
      {
        title: 'About',
        url: '/pages/about',
      },
      {
        title: 'Contact',
        url: '/pages/contact',
      },
      {
        title: 'News',
        url: '/pages/news',
      }
    ]
  }
}
