module.exports = {

  init() {

    $('.rp_modal__toggle').each((index, element) => {

      const $toggle = $(element);
      const $targetModalId = $toggle.data('target');
      const $targetModal = $(`#${$targetModalId}`);
      const $modalClose = $targetModal.find('.rp_modal__close');

      $toggle.on('click', e => {
        e.preventDefault();
        $targetModal.fadeIn();
      });

      $modalClose.on('click', e => {
        e.preventDefault();
        e.stopPropagation();
        $targetModal.fadeOut();
      });

    });

  }

}
