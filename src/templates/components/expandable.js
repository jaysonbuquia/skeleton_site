import Collapsible from '../../scripts/modules/Collapsible';

export default {

  instances: [],

  init: function () {

    let _this = this;
    
    $('.rp_expandable').each((index, element) => {

      _this.instances.push(new Collapsible(element, {
        toggle: '.rp_expandable__toggle',
        body: '.rp_expandable__hidden',
      }));

    });

  }

}
