// Import packages
import $ from 'jquery';

// Import modules
import expandable from '../templates/components/expandable.js';
import teamList from '../templates/components/team-list.js';
import locationTabs from '../templates/components/location-tabs.js';
import modal from '../templates/components/modal.js';


// Declare globals
window.$ = window.jQuery = $;



// Initialize modules
$(document).ready(() => {

  [

    // List down modules to initialize
    expandable,
    teamList,
    locationTabs,
    modal,

  ].map( module => {
    if (module && typeof module.init == 'function') {
      module.init();
    }
  });

});
