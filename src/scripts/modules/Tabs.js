import Component from './Component';

const CLASSNAMES = {
  activeTab: 'active',
  activePane: 'active',
};

export default class Tabs extends Component {

  constructor($element, {tab = '', pane = ''}) {
    super($element);

    this.$tabs = this.$element.find(tab);
    this.$panes = this.$element.find(pane);

    this.bindElements();
  }


  bindElements() {
    const _this = this;

    this.$tabs.on('click', function () {
      _this.select($(this).index());
    });
  }

  select(index = 0) {

    const $targetTab = this.$tabs.eq(index);
    const $targetPane = this.$panes.eq(index);

    // Deactivate others first
    this.$tabs.not($targetTab).add(
    this.$panes.not($targetPane)
    ).removeClass(CLASSNAMES.activeTab);

    // Active target tab and pane
    $targetTab.add($targetPane).addClass(CLASSNAMES.activePane);

  }
}
