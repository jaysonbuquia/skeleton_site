export default class Component {

  constructor($element) {

    // Ensure jQuery object
    this.$element = $($element);

    this.constructor.instances = this.constructor.instances || [];
    this.constructor.instances.push(this);
  }

}
