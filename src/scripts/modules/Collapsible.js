import Component from './Component';

const CLASSNAMES = {
  expanded: 'expanded',
}

export default class Collapsible extends Component {

  constructor($element, {toggle, body}) {
    super($element);

    this.$toggle = this.$element.find(toggle);
    this.$body = this.$element.find(body);
    this.$accordionParent = this.$element.parents('.rp_accordion');

    this.bindElements();
    this.$element[0].Collapsible = this;
  }


  bindElements() {
    this.$toggle.on('click', e => {
      e.preventDefault();
      
      if ( this.$element.hasClass(CLASSNAMES.expanded) ) {
        this.collapse();
      } else {
        this.expand();
      }
    });
  }


  collapse() {
    this.$element.removeClass(CLASSNAMES.expanded);
    this.$body.slideUp();
  }


  expand() {

    console.log('expand');

    // Close others
    if ( this.$accordionParent.length ) {
      this.$accordionParent.find(`.${CLASSNAMES.expanded}`).each((index, el) => {
        if ( el.Collapsible ) el.Collapsible.collapse();
      });
    }

    this.$body.slideDown();
    this.$element.addClass(CLASSNAMES.expanded);
  }

}
